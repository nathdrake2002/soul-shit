﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Medical_Memories
{
    class Program
    {
        // Lists 
        static List<stuff> tool = new List<stuff>();
        static List<stuff> Cart = new List<stuff>();
        static List<sales> Foods = new List<sales>();
        
        // Struts 
        struct stuff
        {
            public string name;
            public int price;
            public int amount;
        }
        struct sales
        {
            public string NameSales;
            public int PaidSales;
            public int QuantitySales;
            public string DateSales;
            public int ChangeSales;

        }

        //when you press a in menue it will take you here
        static void Make_sale()
        {
            int paid;
            int max = 0;

            string choice;
            Console.Clear();
            Console.WriteLine("Welcome to the sales screen! Take some souls then leave");
            Console.WriteLine();
            Console.WriteLine(" (A) Add soul to cart");
            Console.WriteLine(" (B) Remove soul from cart");
            Console.WriteLine();
            Console.WriteLine(" (F) Finalise sale");
            Console.WriteLine(" (X) Return to menu");
            Console.WriteLine();
            Console.WriteLine("Current items in cart:");
            Console.WriteLine();

            for (int i = 0; i < Cart.Count; i++)
            {
                Console.WriteLine($" {Cart[i].name} ${Cart[i].price} x {Cart[i].amount} = ${Cart[i].price * Cart[i].amount}");
            }
            Console.WriteLine();
            Console.Write("Choice: ");
            choice = (Console.ReadLine().ToLower());
            Console.Clear();

            if (choice == "a")
            {
                stuff newstuff = new stuff();
                Console.WriteLine("Here is the available stock");
                Console.WriteLine();
                for (int i = 0; i < tool.Count; i++)
                {
                    Console.WriteLine($"({i}) {tool[i].name} ${tool[i].price}");
                }
                Console.WriteLine();
                Console.Write("Enter amount of souls you want to take ;): ");

                int z = int.Parse(Console.ReadLine());
                Console.Write("Amount: ");
                newstuff.amount = int.Parse(Console.ReadLine());
                newstuff.name = tool[z].name;
                newstuff.price = tool[z].price;
                tool[z] = newstuff;
                Cart.Add(tool[z]);
                Make_sale();
            }

            if (choice == "b")
            {
                Console.WriteLine("Here are your cart items");
                for (int i = 0; i < Cart.Count; i++)
                {
                    Console.WriteLine($"({i}) {Cart[i].name} ${Cart[i].price} x {Cart[i].amount} = ${Cart[i].price * Cart[i].amount}");
                }
                Console.WriteLine();
                Console.WriteLine("Enter the amount of the souls to be removed: ");
                var z = int.Parse(Console.ReadLine());
                Cart.RemoveAt(z);
                Console.WriteLine("Soul removed from cart. Press enter to continue :)");
                Make_sale();
            }

            if (choice == "f")
            {
                for(int i = 0; i < Cart.Count; i++)
                {
                    max += Cart[i].price * Cart[i].amount;
                }

                Console.WriteLine("Checking out");

            }
        }

        // when you press c in menue it will take you here
        static void stock()
        {
            string choice;

            Console.Clear();
            Console.Write("What would you like to do with the ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("ITEMS ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("today");
            Console.WriteLine();
            Console.WriteLine(" (A) Add a new item");
            Console.WriteLine(" (B) Edit an existing item");
            Console.WriteLine(" (C) View list of items");
            Console.WriteLine(" (D) Remove an existing item");
            Console.WriteLine();
            Console.WriteLine(" (X) Go back to main menu");
            Console.WriteLine();
            Console.Write("Choice: ");
            choice = (Console.ReadLine().ToLower());
            Console.Clear();

            if (choice == "a")
            {
                stuff Addedstuff = new stuff();
                Console.WriteLine("Whos soul would you like to add");
                Console.WriteLine();
                Console.Write(" Name: ");
                Addedstuff.name = (Console.ReadLine());
                Console.Write(" Price: $");
                Addedstuff.price = int.Parse(Console.ReadLine());
                tool.Add(Addedstuff);
                stock();
                Console.Clear();
            }

            if (choice == "b")
            {
                Console.WriteLine("Which soul would you like to change");

                for (int i = 0; i < tool.Count; i++)
                {
                    Console.WriteLine($"({i}) {tool[i].name} ${tool[i].price}");
                }
                Console.WriteLine();
                Console.Write("Enter the number of the soul you would like to change: ");
                int a = int.Parse(Console.ReadLine());
                stuff newstuff = new stuff();
                Console.Write(" New Name: ");
                newstuff.name = Console.ReadLine();
                Console.Write(" New Price: $");
                newstuff.price = int.Parse(Console.ReadLine());
                tool[a] = newstuff;
                stock();
                Console.Clear();
            }

            if (choice == "c")
            {
                Console.WriteLine("Here are all your souls: ");
                Console.WriteLine();
                foreach (stuff Addedstuff in tool)
                {
                    Console.WriteLine($" {Addedstuff.name} ${Addedstuff.price}");
                }
                Console.WriteLine("");
                Console.Write("Press enter to continue ;)");
                Console.ReadLine();
                stock();
                Console.Clear();
            }

            if (choice == "d")
            {
                Console.WriteLine("Which soul you want to Terminate ?");
                Console.WriteLine();
                for (int i = 0; i < tool.Count; i++)
                {
                    Console.WriteLine($"({i}) {tool[i].name} ${tool[i].price}");
                }
                Console.WriteLine();
                Console.Write("Enter the number of soul you want to Terminate ");
                int a = int.Parse(Console.ReadLine());
                Console.WriteLine();
                Console.Write("Are you sure you want to Terminate this soul (y/n)? ");
                choice = (Console.ReadLine().ToLower());
                Console.WriteLine();

                if (choice == "y")
                {
                    tool.RemoveAt(a);
                    Console.WriteLine("soul Terminated. Press enter to continue ;)");
                    Console.ReadLine();
                    stock();
                    Console.Clear();
                }

                if (choice == "n")
                {
                    Console.WriteLine("Termination cancelled. Press enter to continue ;)");
                    Console.ReadLine();
                    stock();
                    Console.Clear();
                }
            }

            if (choice == "x")
            {
                Console.Clear();
                menue();
            }
        }

        // the app starts here 
        static void menue()
        {
            Console.Write(" Welcome to ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write("Soul Sales ");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(";)");

            string choice;
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(" What would you like to do today :)");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.Write(" (A) ");
            Console.WriteLine("")
            Console.WriteLine(" (B) Check daily sales");
            Console.WriteLine(" (C) Manage stock");
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine(" (X) Exit");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.Write(" Choice: ");
            Console.ForegroundColor = ConsoleColor.DarkYellow
                ;
            choice = (Console.ReadLine().ToLower());

            if (choice == "a")
            {
                Make_sale();
            }

            else if (choice == "b")
            {

            }

            else if (choice == "c")
            {
                stock();
            }

            else if (choice == "x")
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("                                               We will see you soon ;)");
                Thread.Sleep(1000);
                return;
            }
        }

        static void Main(string[] args)
        {
            menue();
        }
    }
}
